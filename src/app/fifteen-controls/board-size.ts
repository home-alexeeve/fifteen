export class BoardSize{
    num: number;
    name: string;
}

export const BOARD_SIZES: Array<BoardSize> = [
    {num: 3, name: '3 x 3'},
    {num: 4, name: '4 x 4'},
    {num: 5, name: '5 x 5'},
    {num: 6, name: '6 x 6'}
];