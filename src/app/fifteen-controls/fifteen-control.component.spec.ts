import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FifteenControlComponent } from './fifteen-control.component';

describe('FifteenControlComponent', () => {
  let component: FifteenControlComponent;
  let fixture: ComponentFixture<FifteenControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FifteenControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FifteenControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
