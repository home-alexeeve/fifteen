import { Component } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { BoardController } from '../fifteen-core/board-controller';
import { GameLevel } from '../fifteen-core/game-level';
import { Cell } from '../fifteen-core/cell';
import { BOARD_SIZES } from './board-size';

@Component({
  selector: 'app-root',
  styleUrls: ['./controls.css'],
  templateUrl: './fifteen-control.component.html',
  animations: [
    trigger('gameState', [
      state('show', style({
        opacity: 1,
        height: '150px'
      })),
      state('hide', style({
        opacity: 0,
        display: "none",
        height: 0,
      })),
      transition('hide => show', animate('600ms ease-out')),
    ])
  ]
})
export class FifteenControlComponent {
  title = 'Game of Fifteen';

  sizes = BOARD_SIZES;
  selectedSize = this.sizes[0];

  private counter = 0;
  congradulations: string;

  private showIntroduceControls = true;
  private gameIsOver = false;


  cells: Cell[];

  constructor(private boardController: BoardController) { }
  onSizeChange() {
    console.log(this.selectedSize)
    this.selectedSize = this.selectedSize;
  }

  onChanged(isMoved: boolean) {
    if (isMoved) {
      this.counter++;
    }

    this.gameIsOver = this.boardController.isGameOver();
    this.congradulations = this.gameIsOver ? 'Congradulations, game is over!' : '';
  }

  restartGame() {
    this.cells = null;
    this.instantiate(this.boardController.getBackup());
  }

  newGame(level?: string) {
    if (level == undefined) {
      this.showIntroduceControls = true;
    } else {
      this.boardController.buildBoard(GameLevel[level], this.selectedSize.num)
      this.showIntroduceControls = false;
    }

    this.instantiate(this.boardController.getCells());
  }

  private instantiate(cells: Cell[]) {
    this.gameIsOver = false;
    this.counter = 0;
    this.congradulations = '';
    this.cells = cells;
  }

  get initial() {
    return this.showIntroduceControls && !this.gameIsOver ? 'show' : 'hide';
  }

  get inGame() {
    return !this.showIntroduceControls && !this.gameIsOver ? 'show' : 'hide';
  }

  get gameOver() {
    return (!this.showIntroduceControls && this.gameIsOver) ? 'show' : 'hide';
  }

}
