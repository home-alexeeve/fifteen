import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FifteenBoardComponent } from './fifteen-board.component';

describe('FifteenBoardComponent', () => {
  let component: FifteenBoardComponent;
  let fixture: ComponentFixture<FifteenBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FifteenBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FifteenBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
