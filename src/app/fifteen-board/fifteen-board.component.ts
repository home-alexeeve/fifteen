import { Component, Output, EventEmitter, Input } from '@angular/core';
import * as $ from "JQuery";

import { CellAnimationController } from 'src/app/fifteen-core/cell-animation-controller';
import { BoardController } from 'src/app/fifteen-core/board-controller';
import { Position } from '../fifteen-core/position';
import { Cell } from '../fifteen-core/cell';
import { CellViewModel } from '../fifteen-core/cell-view-model';

@Component({
  selector: 'app-fifteen-board',
  template: `<div class="board" id="gameboard">
                <app-fifteen-cell [cells]="boardCells" (onClicked)="onChanged($event)"></app-fifteen-cell>
            </div>`,
  styles: [`.board {min-width: 420px; min-height: 420px; position: relative;}`]
})
export class FifteenBoardComponent{
  @Input()
  boardCells : Cell[];
  constructor(private boardController : BoardController) { }

  @Output()
  onMoved = new EventEmitter<boolean>();

  onChanged(cellName: string) {
    let item = document.getElementById(`cell_${cellName}`);

    if (item != undefined) {
      var cell = this.boardController.getCells().find(c => c.name == cellName);
      var direction = cell.isMovableTo;

      if (this.boardController.moveCell(cell)) {
        let currentPos = this.getCurrentPos(item);
        let cellModel = new CellViewModel(item, currentPos, cell.getStyleDim());
        
        var animationController = new CellAnimationController(cellModel, this.boardController.cellSize)
        animationController.moveTo(direction);
        
        this.onMoved.emit(true);
      }
    }

    this.onMoved.emit(false);
  }
  
  private getCurrentPos(item: HTMLElement): Position {
    return new Position(parseInt($('#' + item.id).css('left').replace(/[^-\d\.]/g, '')),
      parseInt($('#' + item.id).css('top').replace(/[^-\d\.]/g, '')));
  }
}
