import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { FifteenBoardComponent } from './fifteen-board/fifteen-board.component';
import { FifteenControlComponent } from './fifteen-controls/fifteen-control.component';
import { FifteenCellsComponent } from './fifteen-cells/fifteen-cells.component';
import { BoardController } from './fifteen-core/board-controller';

@NgModule({
  declarations: [
   FifteenBoardComponent,
   FifteenControlComponent,
   FifteenCellsComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  bootstrap: [FifteenControlComponent],
  providers: [BoardController]
})

export class AppModule { }
