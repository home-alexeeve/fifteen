import { Injectable } from '@angular/core';

import { Direction } from "./direction"
import { GameLevel } from "./game-level"
import { Cell } from "./cell"
import { Position } from "./position"


@Injectable()
export class BoardController {
    private readonly boardSize = 420;
    public cellSize: number;
    private cnt: number;

    private linearizedBoard: Cell[];
    private boardBackup: Cell[];
    private initialState: Cell[];


    showBoard(): void {
        let sorted = this.linearizedBoard.sort((a, b) => {
            const xDiff = a.pos.y - b.pos.y;
            if (xDiff) return xDiff;
            return a.pos.x - b.pos.x;
        });

        for (let r = 0; r < this.cnt; r++) {
            console.log(
                sorted.filter((_c, i) => i >= this.cnt * r)
                .filter((_u, i) => i < this.cnt)
                .map(it =>it.name + (it.name.length < 2 ? ' ' : '') + ` (${it.pos.x},${it.pos.y})`
            ).join(' '));
        }
        console.log('--------------------------------');
    }

    moveCell(cell: Cell): boolean {
        if (cell.isMovableTo != null) {

            let emptyCell = this.linearizedBoard.filter(c => c.name == '0')[0];
            let initPos = new Position(cell.pos.x, cell.pos.y);

            emptyCell.pos = initPos;

            switch (cell.isMovableTo) {
                case Direction.Top:
                    cell.pos.y--;
                    break;
                case Direction.Right:
                    cell.pos.x++;
                    break;
                case Direction.Down:
                    cell.pos.y++;
                    break;
                case Direction.Left:
                    cell.pos.x--;
                    break;
            }

            console.log(`cell ${cell.name}${cell.name.length < 2 ? ' ' : ''} 
                was moved from (${initPos.x},${initPos.y}) 
                to (${cell.pos.x},${cell.pos.y}) - (${Direction[cell.isMovableTo]})`);

            this.linearizedBoard.forEach(c => c.isMovableTo = this.isMovable(c));
            return true;
        }

        return false;
    }

    public buildBoard(level: GameLevel, cnt: number): void {
        this.cnt = cnt;
        this.cellSize = this.boardSize / this.cnt;
        let cellsMatrix: Cell[][];
        cellsMatrix = [];
        var name = 0;
        for (var y: number = 0; y < cnt; y++) {
            cellsMatrix[y] = [];
            for (var x: number = 0; x < cnt; x++) {
                
                cellsMatrix[y][x] = new Cell(`${name++}`, new Position(x, y), this.cellSize);
            }
        }
        this.linearizedBoard = cellsMatrix.reduce((pn, u) => [...pn, ...u], []);
        this.initialState = this.deepCopy(this.linearizedBoard);
        this.shuffle(level);
        this.boardBackup = this.deepCopy(this.linearizedBoard);
    }

    private shuffle(level: GameLevel) {
        this.linearizedBoard.forEach(c => {
            c.isMovableTo = this.isMovable(c);
        });

        let movable = this.linearizedBoard.filter(c => c.isMovableTo != null);
        let cell = movable[Math.floor(Math.random() * movable.length)];
        for (let i = 0; i < level * this.cnt; i++) {
            this.moveCell(cell);

            movable = this.linearizedBoard.filter(c => c.isMovableTo != null);
            var ncell = movable[Math.floor(Math.random() * movable.length)];
            while (ncell == cell) {
                ncell = movable[Math.floor(Math.random() * movable.length)];
            }
            cell = ncell;
        }
        this.linearizedBoard.forEach(c => {
            c.isMovableTo = this.isMovable(c);
        });
        console.log('--------Cells are mixed--------');
    }

    getCells(): Cell[] {
        return this.linearizedBoard;
    }

    getBackup(): Cell[] {
        this.linearizedBoard = this.deepCopy(this.boardBackup);
        return this.linearizedBoard;
    }

    public isGameOver(): boolean {
        let status = true;
        this.linearizedBoard.forEach(el => {
            let etalon = this.initialState.filter(c => c.name == el.name)[0];
            if (el.pos.x != etalon.pos.x
                || el.pos.y != etalon.pos.y) {
                status = false;
            }
        });

        return status;
    }

    private isMovable(cell: Cell): Direction {
        let canMove: Direction = null;

        if (cell.pos.y > 0 && !this.linearizedBoard.some(c => c.pos.x == cell.pos.x && c.pos.y == cell.pos.y - 1 && c.name != '0'))
            return Direction.Top;
        if (cell.pos.x < this.cnt - 1 && !this.linearizedBoard.some(c => c.pos.x == cell.pos.x + 1 && c.pos.y == cell.pos.y && c.name != '0'))
            return Direction.Right;
        if (cell.pos.y < this.cnt - 1 && !this.linearizedBoard.some(c => c.pos.x == cell.pos.x && c.pos.y == cell.pos.y + 1 && c.name != '0'))
            return Direction.Down;
        if (cell.pos.x > 0 && !this.linearizedBoard.some(c => c.pos.x == cell.pos.x - 1 && c.pos.y == cell.pos.y && c.name != '0'))
            return Direction.Left;

        return canMove;
    }

    private deepCopy(source: Cell[]): Cell[] {
        let copy = new Array<Cell>();
        for (let el of source) {

            let cell = new Cell(el.name, new Position(el.pos.x, el.pos.y), el.size);
            cell.isMovableTo = el.isMovableTo;
            copy.push(cell);
        };

        return copy;
    }
}
