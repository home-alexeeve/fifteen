import { Direction } from "./direction";
import { CellViewModel } from "./cell-view-model";

export class CellAnimationController {

    private readonly cellModel: CellViewModel;

    constructor(cellModel: CellViewModel, public velocity: number = 100) {
        this.cellModel = cellModel;
    }

    public moveTo(direction: Direction) {

        var model = this.cellModel;
        var x = model.pos.x;
        var y = model.pos.y;
        var bounceEaseOut = this.makeEaseOut(this.bounce);
        this.animate({
            duration: 500,
            timing: bounceEaseOut,
            draw: function (progress) {
                model.draw(direction, progress, x, y)
            }
        });
    }

    private animate({ timing, draw, duration }) {

        let start = performance.now();
        let v= this.velocity;
        requestAnimationFrame(function animate(time) {
            let timeFraction = (time - start) / duration;
            if (timeFraction > 0.95) timeFraction = 1;

            let progress = timing(timeFraction) * v;

            draw(progress);

            if (timeFraction < 0.985) {
                requestAnimationFrame(animate);
            }

        });
    }

    private makeEaseOut(timing) {
        return function (timeFraction) {
            return 1 - timing(1 - timeFraction);
        }
    }

    private bounce(timeFraction) {
        for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (timeFraction >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * timeFraction) / 4, 2) + Math.pow(b, 2)
            }
        }
    }
}

