import { Direction } from "./direction"
import { Position } from "./position"

export class Cell {

    constructor(public name: string,
        public pos: Position, 
        public size: number = 100, 
        public isMovableTo: Direction = null ) {
    }

    public getStyleDim(): string{
        return  `${this.size + 'px'}`;
    }

    public getStyleLeft(): string{
        return  `${this.pos.x*this.size + 'px'}`;
    }

    public getStyleTop(): string{
        return  `${this.pos.y*this.size + 'px'}`;
    }

    public getId(): string{
        return  `cell_` + `${this.name}`;
    }
}