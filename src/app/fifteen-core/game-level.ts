export enum GameLevel {
    Easy = 5,
    Medium = 10,
    Hard = 20
}