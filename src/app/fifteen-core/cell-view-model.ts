import { Direction } from "./direction";
import { Position } from '../fifteen-core/position';

export class CellViewModel {
    constructor(
        private htmlelm: HTMLElement,
        public pos: Position,
        private dim: string) {
        this.htmlelm.style.width = this.htmlelm.style.height = this.dim;
    }

    draw(direction: Direction, progress: number, startX: number, startY: number) {
        switch (direction) {
            case Direction.Top:
                this.pos.y = startY - progress;
                break;
            case Direction.Down:
                this.pos.y = startY + progress;
                break;
            case Direction.Left:
                this.pos.x = startX - progress;
                break;
            case Direction.Right:
                this.pos.x = startX + progress;
                break;
        }

        this.htmlelm.style.top = this.pos.y + 'px';
        this.htmlelm.style.left = this.pos.x + 'px';
    }
}