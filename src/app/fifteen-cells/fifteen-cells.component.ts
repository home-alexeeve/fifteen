import { Component, Output, EventEmitter, Input } from '@angular/core';
import { BoardController } from 'src/app/fifteen-core/board-controller';
import { Cell } from 'src/app/fifteen-core/cell';
   
@Component({
  selector: 'app-fifteen-cell',
  templateUrl: './fifteen-cells.component.html',
  styleUrls: ['./fifteen-cells.component.css']
})
export class FifteenCellsComponent {
  @Input()
  cells: Cell[];

  constructor(private boardController: BoardController) { }
  
  @Output()
  onClicked = new EventEmitter<string>();

  move(name: string){
    this.onClicked.emit(name);
  }
}
