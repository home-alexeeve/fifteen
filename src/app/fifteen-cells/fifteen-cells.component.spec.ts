import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FifteenCellsComponent } from './fifteen-cells.component';

describe('FifteenCellsComponent', () => {
  let component: FifteenCellsComponent;
  let fixture: ComponentFixture<FifteenCellsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FifteenCellsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FifteenCellsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
